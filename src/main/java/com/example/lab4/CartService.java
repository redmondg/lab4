package com.example.lab4;

import java.util.Map;

public interface CartService {

    void addToCart(int id, int num);
    void removeFromCart(int id);
    Map<Integer, Integer> getAllItemsInCart();
    double calculateCartCost();
    String getEmail();
    double calculateSalesTax();
    double calculateDeliveryCharge();


}
