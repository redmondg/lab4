package com.example.lab4;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;


import java.util.HashMap;
import java.util.Map;

@SpringBootApplication
public class Application {

	public static void main(String[] args) {


		ApplicationContext context = SpringApplication.run(Application.class, args);

		CartService service = context.getBean(CartServiceImpl.class);
		service.addToCart(0,1);
		service.addToCart(1,2);
		service.addToCart(2,1);

		service.removeFromCart(2);

		double cartCost = service.calculateCartCost();
		System.out.printf("Total cart cost = %.2f\n", cartCost);
		double totalCost = service.calculateSalesTax() + service.calculateDeliveryCharge() + cartCost;
		System.out.printf("Total cost after delivery of %.2f and sales tax of %.2f = %.2f\n", service.calculateDeliveryCharge(), service.calculateSalesTax(), totalCost);
		System.out.printf("If you have any issues, contact %s for assistance.\n", service.getEmail());


		ResourcesBean resourcesBean = context.getBean(ResourcesBean.class);
		System.out.println("Profile-specific properties: " + resourcesBean);
		System.out.printf("DB : %s, Logs : %s\n", resourcesBean.getDb(),resourcesBean.getLogs());

	}

	@Bean
	public Map<Integer, Item> catalog(){

		Map<Integer, Item> items = new HashMap<>();
		items.put(0, new Item(0, "Camera", 1599.00));
		items.put(1, new Item(1, "Laptop", 3199.99));
		items.put(2, new Item(2, "Car", 43000.59));


		return items;
	}
}
