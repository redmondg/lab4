package com.example.lab4;

import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
public class CartRepositoryImpl implements CartRepository {

    private Map<Integer, Integer> cart = new HashMap<>();

    @Override
    public void add(int k, int v) {
        int count = cart.containsKey(k) ? cart.get(k)+v : 1;
        cart.put(k,count);
    }

    public void remove(int k){
        cart.remove(k);
    }

    public Map<Integer, Integer> getAll(){
        return cart;
    }
}
