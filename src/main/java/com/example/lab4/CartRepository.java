package com.example.lab4;

import java.util.Map;

public interface CartRepository {
    void add(int k, int v);
    void remove(int k);
    Map<Integer, Integer> getAll();
}
